// api url 
const api_url =  
      "https://restcountries.eu/rest/v2/all"; 

let search = document.getElementById('search');
let search_term;
let data;
let cinfo;
  
// Defining async function 
async function getapi(url) { 
    
    // Storing response 
    const response = await fetch(url); 
    
    // Storing data in form of JSON 
    data = await response.json(); 
    if (response) { 
        console.log('getapi query passed')
    } 
    show(data); 
} 

// Calling that async function 
getapi(api_url); 

function show(data) { 
    let tab = '';
    
    // Loop for accesing rows  
    for (let r of data) { 
        tab +=  
        `
        <div class="card mb-3">
            <div class="row g-0">
              <div class="col-md-5">
                <img src="${r.flag}" alt="..." class="img-fluid" >
              </div>
              <div class="col-md-7">
                <div class="card-body">
                  <h5 class="card-title">${r.name}</h5>
                  <p class="card-text">Currencies: ${r.currencies[0].name}</p>
                  <p class="card-text">Timezones: ${r.timezones}</p>
                   
                    <!--  buttons 
                    <div class="row">
                    <div class="col-md-6 mt-2">
                    <a href="https://www.google.co.in/maps/place/${data[0].name}/" class="btn btn-primary">Show Map </a>
                    </div>
                    <div class="col-md-6 mt-2">

                     <a href="details.html">
                     <button onclick="detailinfo(this.value)" value="${r.name}" id="buttonId"  class="btn btn-primary">Details</button>
                     </a>

                    </div>


                    </div> -->
                    <div class="row">
                            <div class="col-sm-6">
                                <a  class=" btn-outline-primary" href="https://www.google.co.in/maps/place/${data[0].name}/">
                                    <button type="button" class="btn-outline-primary ">
                                        <h7>Show Map</h7>
                                    </button>
                                </a></div>
                            <div class="col-sm-6">
                                <a href="details.html" class="btn-outline-primary ">
                                    <button onclick="detailinfo(this.value)" value="${r.name}" id="buttonId" type="button" class="btn-outline-primary ">
                                        <h7>Details</h7>
                                    </button>
                                </a>
                            </div>
                        </div>


                  
                </div>
              </div>
            </div>
          </div>

        `;

    } 
  
    document.getElementById("countries").innerHTML = tab; 
} 


search.addEventListener('input', (e) => {
  search_term = e.target.value;

  showResults(search_term.toUpperCase());
});

function showResults(search_term) {
  let countries = document.getElementById('countries');
  resultData = data.filter((country) => {

    return country.name.toUpperCase().startsWith(search_term);
  });

  console.log(resultData);
     
  show(resultData);
}

function detailinfo(val){
  let tab=val;
  console.log(tab)
  window.localStorage.setItem('country', val);
 
}
