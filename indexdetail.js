let selectedCountry=window.localStorage.getItem('country');
console.log(selectedCountry);

async function getInfo(url) { 
    
    const response = await fetch(url); 
    
    
    cinfo = await response.json(); 
    if (response) { 
        console.log('GetInfo query passed')
    } 
   
    showDetails(cinfo);
    showCountryFlags(cinfo);
} 

const api = `https://restcountries.eu/rest/v2/name/${selectedCountry}?fullText=true`
  getInfo(api);
function showCountryFlags(data){
    const countryFlags=[]
    for (border in data[0].borders) {
        const flag = 'https://restcountries.eu/data/' + data[0].borders[border].toLowerCase() + '.svg'
        countryFlags.push(flag);
    }
    console.log(countryFlags, 'country flags')
    let tab = '';
    let f;
    for (f in countryFlags) {
        console.log(countryFlags[f])

        tab +=
            `<!-- <div class="row-sm-12">-->
              <div class="col-md-4">
                <div class="row"><img src="${countryFlags[f]}" alt="..." class="img-fluid " ></div>
                </div>
              
             
               `;
    }
    let detail = document.getElementById('flags');
    document.getElementById('flags').innerHTML=tab;

}

function showDetails(data) {

    let detail = document.getElementById('details');
    detail.innerHTML = `
          <div class="A">
                  <div class="col-sm-4"><h5> ${data[0].name} </h5></div>
              </div>
          <div class="row">
              <div class="col-md-6">
                <div class="row"><img src="${data[0].flag}" alt="..." class="img-fluid max-width: 100%; height: auto;" ></div>
                
              </div>

              <div class="col-md-6">
              <div class="row">
                  <div class="col-sm-5"> <h5>Country:</h5> </div>
                  <div class="col-sm-4"> ${data[0].name} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Currency:</h5> </div>
                  <div class="col-sm-4"> ${data[0].currencies[0].name} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Capital:</h5> </div>
                  <div class="col-sm-4"> ${data[0].capital} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Population:</h5> </div>
                  <div class="col-sm-4"> ${data[0].population} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Region:</h5> </div>
                  <div class="col-sm-4"> ${data[0].region} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Sub-region:</h5> </div>
                  <div class="col-sm-4"> ${data[0].subregion} </div>
              </div>
              <div class="row">
                  <div class="col-sm-5"> <h5>Area:</h5> </div>
                  <div class="col-sm-4"> ${data[0].area} </div>
              
              
          </div>
          
          </div> 
          </div>
          </div>
          `;


    // let falg = document.getElementById('flags');

}
